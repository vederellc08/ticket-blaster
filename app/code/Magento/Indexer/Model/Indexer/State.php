<?php
namespace Magento\Indexer\Model\Indexer;

use Magento\Framework\Indexer\StateInterface;

class State extends \Magento\Framework\Model\AbstractModel implements StateInterface
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'indexer_state';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'indexer_state';

    /**
     * Return indexer id
     *
     * @return string
     */
    public function getIndexerId()
    {
        return parent::getIndexerId();
    }

    /**
     * Set indexer id
     *
     * @param string $value
     * @return $this
     */
    public function setIndexerId($value)
    {
        return parent::setIndexerId($value);
    }    

    /**
     * Return updated
     *
     * @return string
     */
    public function getUpdated()
    {
        return parent::getUpdated();
    }

    /**
     * Set updated
     *
     * @param string $value
     * @return $this
     */
    public function setUpdated($value)
    {
        return parent::setUpdated($value);
    }

    public function loadByIndexer($indexerId)
    {

    }

    /**
     * Status setter
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        return parent::setStatus($status);
    }

    /**
     * Return status
     *
     * @return string
     */
    public function getStatus()
    {
        return parent::getStatus();
    }
}
